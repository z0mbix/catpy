#!/usr/bin/env python

import sys


def cat(filename):
    print filename, '======='
    f = open(filename, 'r')
    for line in f:
        print line,
    f.close()


def main():
    args = sys.argv[1:]
    for filename in args:
        cat(filename)


if __name__ == '__main__':
    main()