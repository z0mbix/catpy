#!/bin/bash
#
# Very simple initial code. This can be greatly improved :)
#
# z0mbix - 23 Oct 2013
#
# TODO:
#  1. Publish to yum repo on succesful build
#  2. ?
#

if [ -z $ARTIFACT_DIR ]; then
  echo "I don't know where to put my build artifacts!"
  exit 99
fi

if [ -z $VERSION ]; then
  echo "I don't know what verison to build!"
  exit 98
fi

if [ -z $BUILD_NUMBER ]; then
  echo "I don't know what build number to use!"
  exit 97
fi

if [ -z $TMP_DIR ]; then
  echo "I don't appear to have a project specific temp directory!"
  exit 96
fi

if [ -z $BUILD_DIR ]; then
  echo "I don't know where to build stuff!"
  exit 95
fi

RPM_ROOT="rpmbuild"
TMP_SPEC="${TMP_DIR}/${JOB_NAME}.tmp.spec"

echo
echo "===================================================================="
echo WORKSPACE $WORKSPACE
echo JENKINS_HOME $JENKINS_HOME
echo ARTIFACT_DIR $ARTIFACT_DIR
echo REPO_DIR $REPO_DIR
echo BUILD_DIR $BUILD_DIR
echo BUILD_NUMBER $BUILD_NUMBER
echo BUILD_ID $BUILD_ID
echo BUILD_DISPLAY_NAME $BUILD_DISPLAY_NAME
echo JOB_NAME $JOB_NAME
echo PROJECT_NAME $PROJECT_NAME
echo BUILD_TAG $BUILD_TAG
echo TMP_DIR $TMP_DIR
echo "===================================================================="
echo

cd $WORKSPACE

if [ -f $ARTIFACT_DIR/${PROJECT_NAME}-${VERSION}-${BUILD_NUMBER}.tar.gz ]; then
  echo "Warning: file already exists: ${PROJECT_NAME}-${VERSION}-${BUILD_NUMBER}.tar.gz"
fi

echo "Building source package for ${PROJECT_NAME}"
tar -cvzf $ARTIFACT_DIR/${PROJECT_NAME}-${VERSION}-${BUILD_NUMBER}.tar.gz $REPO_DIR/src/catpy.py
if [ $? -ne "0" ]; then
	echo "Could not build source tarball!"
	exit 1
fi

sleep 5

echo "Tarball created: ${PROJECT_NAME}-${VERSION}-${BUILD_NUMBER}.tar.gz"
